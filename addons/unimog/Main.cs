#if TOOLS
using Godot;
using System;

[Tool]
public class Main : EditorPlugin
{
    private Control _panelInstance;

    public override void _EnterTree()
    {
        base._EnterTree();
        var packedScene = ResourceLoader.Load<PackedScene>("res://addons/unimog/interface.tscn");
        _panelInstance = (Control)packedScene.Instance();
        //_panelInstance = (Control)GD.Load<PackedScene>("res://addons/unimog/interface.tscn").Instance();
        AddControlToDock(DockSlot.LeftBr, _panelInstance);
        //GetEditorInterface().GetEditorViewport().AddChild(_panelInstance);
        //MakeVisible(false);
    }

    public override void _ExitTree()
    {
        base._ExitTree();
        RemoveControlFromDocks(_panelInstance);
        _panelInstance.Free();
        if (_panelInstance != null)
        {
            RemoveControlFromDocks(_panelInstance);
            _panelInstance.QueueFree();
        }
    }

    /*
    public override bool HasMainScreen()
    {
        return true;
    }

    public override void MakeVisible(bool visible)
    {
        if (_panelInstance != null)
        {
            _panelInstance.Visible = visible;
        }
    }
    
    public override string GetPluginName()
    {
        return PluginName;
    }

    public override Texture GetPluginIcon()
    {
        return GetEditorInterface().GetBaseControl().GetIcon("Node", "EditorIcons");
    }
    */
    
}
#endif
