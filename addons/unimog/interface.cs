#if TOOLS

using Godot;
using System;
using System.IO;
using Directory = System.IO.Directory;

[Tool]
public class TestInterface : Control
{
    // Declare member variables here. Examples:
    // private int a = 2;
    // private string b = "text";

    // Called when the node enters the scene tree for the first time.
    public override void _Ready()
    {
        
    }

    private void TestMethod()
    {
        var globalPath = ProjectSettings.GlobalizePath("res://");

        Directory.CreateDirectory(globalPath + "_raw");
        Directory.CreateDirectory(globalPath + "_raw/sprites");
        Directory.CreateDirectory(globalPath + "_raw/models");
        Directory.CreateDirectory(globalPath + "_raw/textures");
        Directory.CreateDirectory(globalPath + "_raw/sound");
        
        Directory.CreateDirectory(globalPath + "assets");
        Directory.CreateDirectory(globalPath + "assets/sprites");
        Directory.CreateDirectory(globalPath + "assets/models");
        Directory.CreateDirectory(globalPath + "assets/textures");
        Directory.CreateDirectory(globalPath + "assets/music");
        Directory.CreateDirectory(globalPath + "assets/sfx");
        Directory.CreateDirectory(globalPath + "assets/font");
        
        Directory.CreateDirectory(globalPath + "src");
        Directory.CreateDirectory(globalPath + "src/controller");
        Directory.CreateDirectory(globalPath + "src/controller/player");
        Directory.CreateDirectory(globalPath + "src/entities");
        Directory.CreateDirectory(globalPath + "src/gui");
        Directory.CreateDirectory(globalPath + "src/gui/pause");
        Directory.CreateDirectory(globalPath + "src/gui/hud");
        Directory.CreateDirectory(globalPath + "src/gui/main_menu");
        Directory.CreateDirectory(globalPath + "src/gui/options");
        Directory.CreateDirectory(globalPath + "src/sfx");
        Directory.CreateDirectory(globalPath + "src/world");
    }
}

#endif