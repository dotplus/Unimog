#if TOOLS
using Godot;
using File = System.IO.File;

[Tool]
public class AddNodeTemplate : Node
{
    private string _nodeName;
    private string _path;
    
    private void AddToFileSystem()
    {
        var globalPath = ProjectSettings.GlobalizePath("res://");
        File.Copy(_nodeName, File.Exists(globalPath + _path + _nodeName) ? _nodeName : globalPath);
    }
}
#endif
