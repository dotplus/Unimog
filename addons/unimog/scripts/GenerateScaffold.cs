#if TOOLS
using Godot;
using Directory = System.IO.Directory;

[Tool]
public class GenerateScaffold : Node
{
    private void GenerateGenericProjectScaffold()
    {
        var globalPath = ProjectSettings.GlobalizePath("res://");

        // Raw editable types such as .xcf, .asperite, .blend, etc
        Directory.CreateDirectory(globalPath + "_raw");
        Directory.CreateDirectory(globalPath + "_raw/sprites");
        Directory.CreateDirectory(globalPath + "_raw/models");
        Directory.CreateDirectory(globalPath + "_raw/textures");
        Directory.CreateDirectory(globalPath + "_raw/sound");
        
        // Game Assets
        Directory.CreateDirectory(globalPath + "assets");
        Directory.CreateDirectory(globalPath + "assets/sprites");
        Directory.CreateDirectory(globalPath + "assets/models");
        Directory.CreateDirectory(globalPath + "assets/textures");
        Directory.CreateDirectory(globalPath + "assets/music");
        Directory.CreateDirectory(globalPath + "assets/sfx");
        Directory.CreateDirectory(globalPath + "assets/font");

        // Folder Structure
        Directory.CreateDirectory(globalPath + "src");
        Directory.CreateDirectory(globalPath + "src/controller");
        Directory.CreateDirectory(globalPath + "src/controller/player");
        Directory.CreateDirectory(globalPath + "src/entities");
        Directory.CreateDirectory(globalPath + "src/gui");
        Directory.CreateDirectory(globalPath + "src/gui/pause");
        Directory.CreateDirectory(globalPath + "src/gui/hud");
        Directory.CreateDirectory(globalPath + "src/gui/main_menu");
        Directory.CreateDirectory(globalPath + "src/gui/options");
        Directory.CreateDirectory(globalPath + "src/sfx");
        Directory.CreateDirectory(globalPath + "src/world");
    }
}
#endif