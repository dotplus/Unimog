using Godot;
using System;
using System.Collections.Generic;

public class Air : PlayerState
{
    /// <summary>
    /// If we get a message asking us to jump, we jump.
    /// </summary>
    /// <param name="message"></param>
    public override void Enter(Dictionary<string, bool> message = null)
    {
        if (message != null &&
            message.ContainsKey("doJump") &&
            message["doJump"] == true)
        {
            _player.Velocity.y = -_player.JumpImpulse;
        }
    }

    public override void PhysicsUpdate(float delta)
    {
        var inputDirectionX = Input.GetActionStrength("move_right") - Input.GetActionStrength("move_left");

        _player.Velocity.x = _player.Speed * inputDirectionX;
        _player.Velocity.y += _player.Gravity * delta;
        _player.Velocity = _player.MoveAndSlide(_player.Velocity, Vector2.Up);

        // Landing
        if (_player.IsOnFloor())
        {
            if (Mathf.IsEqualApprox(_player.Velocity.x, 0.0f))
            {
                _stateMachine.Transition("Idle");
            }
            else
            {
                _stateMachine.Transition("Run");
            }
        }
    }
}
