using Godot;
using System;
using System.Collections.Generic;

public class Run : PlayerState
{
    public override void PhysicsUpdate(float delta)
    {
        if (!_player.IsOnFloor())
        {
            _stateMachine.Transition("Air");
        }

        // We move the run-specific input code to the state.
        // A good alternative would be to define a `get_input_direction()` function on the `Player.gd`
        // script to avoid duplicating these lines in every script.
        var inputDirectionX = Input.GetActionStrength("move_right") - Input.GetActionStrength("move_left");

        _player.Velocity.x = _player.Speed * inputDirectionX;
        _player.Velocity.y += _player.Gravity * delta;
        _player.Velocity = _player.MoveAndSlide(_player.Velocity, Vector2.Up);

        if (Input.IsActionJustPressed("move_up"))
        {
            var message = new Dictionary<string, bool>()
            {
                { "doJump", true}
            };
            _stateMachine.Transition("Air", message);
        }
        else if (Mathf.IsEqualApprox(inputDirectionX, 0.0f))
        {
            _stateMachine.Transition("Idle");
        }
    }
}
