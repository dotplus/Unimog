using Godot;
using System;
using System.Diagnostics;

public class Player : KinematicBody2D
{
    public int Speed = 500;
    public int JumpImpulse = 1200;
    public int Gravity = 3500;

    public Vector2 Velocity = new Vector2();

    public StateMachine _stateMachine;

    public override void _Ready()
    {
        _stateMachine = GetNode<StateMachine>("StateMachine");
    }

    public override void _PhysicsProcess(float delta)
    {
        return;
    }
}
