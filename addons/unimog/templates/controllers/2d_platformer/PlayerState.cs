using Godot;
using System;
using System.Threading.Tasks;

public class PlayerState : State
{
    protected Player _player;

    public override void _Ready()
    {
        // The states are children of the `Player` node so their `_ready()` callback will execute first. That's why we wait for the `owner` to be ready first.
        Task.Run(async () => await ToSignal(Owner, "ready"));
        
        // Using as keyword casts the owner var. to the Player type. If the owner is not player we get null
        _player = Owner as Player;

        if (_player == null)
        {
            throw new InvalidProgramException("Player is null in PlayerState type check");
        }
    }
}
