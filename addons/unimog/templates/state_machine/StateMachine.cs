using Godot;
using System.Collections.Generic;
using System.Threading.Tasks;

public class StateMachine : Node
{
    [Signal]
    public delegate void Transitioned(string stateName);

    [Export] public NodePath InitialState;
    public State State;

    public override void _Ready()
    {
        base._Ready();
        State = GetNode<State>(InitialState);
        Task.Run(async () => await ToSignal(Owner, "ready"));

        foreach (State child in GetChildren())
        {
            child._stateMachine = this;
        }

        State.Enter();
    }

    /// <summary>
    /// This part of the state machine subscribes to node callbacks and delegates them to state objects 
    /// </summary>
    /// <param name="event"></param>
    public override void _UnhandledInput(InputEvent @event)
    {
        State.HandleInputs(@event);
    }

    public override void _Process(float delta)
    {
        State.Update(delta);
    }

    public override void _PhysicsProcess(float delta)
    {
        State.PhysicsUpdate(delta);
    }
    
    /// <summary>
    /// This function calls the current state exit function, then changes the activate state, and calls its
    /// its enter function. We can pass msg for additional parameters
    /// </summary>
    /// <param name="targetStateName"></param>
    /// <param name="message"></param>

    public void Transition(string targetStateName, Dictionary<string, bool> message = null)
    {
        if (!HasNode(targetStateName)) return;
        
        State.Exit();
        State = GetNode<State>(targetStateName);
        State.Enter(message);
        EmitSignal(nameof(Transition), State.Name);
    }
}
