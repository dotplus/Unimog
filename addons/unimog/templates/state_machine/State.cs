using Godot;
using System;
using System.Collections.Generic;

public class State : Node
{
    /// <summary>
    /// Reference to the state machine to call the TransitionTo method. 
    /// </summary>
    public StateMachine _stateMachine = null;

    /// <summary>
    /// Virtual function. Receives events from _unhandled_input callback
    /// </summary>
    /// <param name="inputEvent"></param>
    public virtual void HandleInputs(InputEvent inputEvent)
    {
        return;
    }

    /// <summary>
    /// Virtual function. Receives events from _process callback
    /// </summary>
    /// <param name="delta"></param>
    public virtual void Update(float delta)
    {
        return;
    }

    /// <summary>
    /// Virtual function. Receives events from _physicsUpdate callback
    /// </summary>
    /// <param name="delta"></param>
    public virtual void PhysicsUpdate(float delta)
    {
        return;
    }

    /// <summary>
    /// Virtual function called by state machine upon changing state. Msg parameter is dict with arb. data the state can
    /// use to init. itself
    /// </summary>
    /// <param name="message"></param>
    public virtual void Enter(Dictionary<string, bool> message = null)
    {
        return;
    }

    /// <summary>
    /// Virtual function called by state machine before changing state. Use this to clean up state data.
    /// </summary>
    public virtual void Exit()
    {
        return;
    }
}
